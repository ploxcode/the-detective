using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public string characterName = "Player";
    public int health = 100;
    public int currency = 10;
    public float walkSpeed = 5f;
    public float horizontal;
    public float vertical;
    Rigidbody2D body;
    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");

        // flip sprite renderer if going left
        if (horizontal < 0)
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
        }

        if (horizontal > 0)
        {
            this.GetComponent<SpriteRenderer>().flipX = false;
        }


    }

    private void FixedUpdate()
    {
        body.velocity = new Vector2(horizontal * walkSpeed, vertical * walkSpeed);
    }
}
