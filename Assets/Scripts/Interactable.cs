using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public string dialouge = "";
    public float interactDistance = 10f;
    bool isInteracting = false;
    public GameObject interactee; // the object it will interact with
    private TextMeshProUGUI TextGameObject;
    // Start is called before the first frame update
    void Start()
    {
        TextGameObject = GameObject.Find("ClueText").GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {

        float distance = Vector3.Distance(this.transform.position, interactee.transform.position);

        // Check if interactee is within range to activate dialouge
        if (distance <= interactDistance)
        {   
            if (TextGameObject != null) 
            {
                TextGameObject.text = dialouge;
                isInteracting = true;
            }
        }

        if (isInteracting && distance > interactDistance)
        {
            if (TextGameObject != null)
            {
                TextGameObject.text = "";
                isInteracting = false;
            }
        }
    }
}
